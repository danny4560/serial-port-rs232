﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication2
{
    class RS232P
    {
        
    private System.IO.Ports.SerialPort port = new System.IO.Ports.SerialPort("COM1",9600);

        public void OpenPort()
        {
            port.Open();
        }

        public void ClosePort()
        {
            port.Close();

        }
        
        public bool Isopened()
        {
            return (port.IsOpen);
        }

        public void Chgbaudrate(int baudrate)
        {
            port.BaudRate = baudrate;
        }

        public void ChgCOM(string COM)
        {
            port.PortName = COM;
        }

        public void SendingCommand(byte[] data)
        {
            port.Write(data, 0, data.Length);
        }

        public double ReadNow()
        {
            double portnow;
            portnow = (double)port.ReadChar();
            return portnow;
        }

    }
}
