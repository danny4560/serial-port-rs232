﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO.Ports;
using System.Timers;


namespace WpfApplication2
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string data1;
       // int baudrate;
        private Timer t;
        // bool COM_changed_first_time = false;
        private System.IO.Ports.SerialPort new_comm = new System.IO.Ports.SerialPort("COM1", 9600);
        public MainWindow()
        {
            InitializeComponent();
            SetPortsToComboBox();
            t = new Timer();//initiallize timer that will update the COM's every 10 sec 
            t.Interval = 10000;
            t.Elapsed += T_Elapsed;
            t.Start();

        }

        

        //private delegate void doSomething(int i);
        //private void printsomething(int index)
        //{
        //    Console.WriteLine(index.ToString());
        //}
        //private event doSomething doSomethingEvent;
        

        private void SetPortsToComboBox()
        {

            string tmp = comboBox_COM.Text;
            comboBox_COM.Items.Clear();
            comboBox_COM.IsEnabled = false;
            string[] ports = GetAllPorts();
            foreach (string str in ports)
                comboBox_COM.Items.Add(str);
            comboBox_COM.IsEnabled = true;
            comboBox_COM.Text = tmp;
        }

        private void T_Elapsed(object sender, ElapsedEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(SetPortsToComboBox);
        }

        public string[] GetAllPorts()
        {
            string[] allPorts = SerialPort.GetPortNames();

            //for (int i = 0; i < allPorts.Length; i++)
            //{
            //    textBox_com.Text += " " + allPorts[i];
            //}
            return allPorts;

        }


        private void button_Send(object sender, RoutedEventArgs e)
        {
            // data1 = this.textBox_Input.Text;
            string data_string=textBox_Input.Text;
            
            //for(int i=0;i<data_to_send.Length;i++)
            //{
            //   // data_send[i] = (byte)(Convert.ToByte(data_to_send.IndexOf[i]));
            //   data_send[i] = data_to_send.in
            //}
            byte[] data_byte = Encoding.ASCII.GetBytes(data_string);//now its ascii and each ascii char gets byte
            if (radioButton_HEX.IsChecked == true)
            {
                // string a= (string)(data_byte[0]);
                //for (int z = 0; z < data_byte.Length; z ++)
                //{
                //    data_byte[z] = (byte)(Convert.ToInt32(data_byte[z]));
                //}
                //char a = (char)(data_byte[0]);
                //data_byte[0] = (byte)(data_byte[0] << 4);
                //data_byte[1] = (byte)(data_byte[1] << 4);

                //    for (int i=0;i<data_byte.Length;i=i+2)   
                //{
                //    data_byte[i] = (byte)(data_byte[i] << 4);
                //    data_byte[i] = (byte)(data_byte[i] | data_byte[i+1]);
                //}

                //new_comm.Write(new byte[] { 0xFA }, 0, 1);
                data_byte = StringToByteArray(textBox_Input.Text);
                new_comm.Write(data_byte, 0, data_byte.Length);

            }
            if (radioButton_DEC.IsChecked == true)
            {
                
            }
            if(radioButton_ASCII.IsChecked==true)
            {

            }
           // new_comm.Write(new byte[] { 0xFA }, 0, 1);
           //new_comm.Write(data_byte, 0, data_byte.Length);
        }
        //covertion functions
        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }
        public void ConvertStringDecimal(string stringVal)
        {
            decimal decimalVal = 0;

            try
            {
                decimalVal = System.Convert.ToDecimal(stringVal);
              //  System.Console.WriteLine(
                //    "The string as a decimal is {0}.", decimalVal);
            }
            catch (System.OverflowException)
            {
              //  System.Console.WriteLine(
                //    "The conversion from string to decimal overflowed.");
            }
            catch (System.FormatException)
            {
              //  System.Console.WriteLine(
                //    "The string is not formatted as a decimal.");
            }
            catch (System.ArgumentNullException)
            {
             //   System.Console.WriteLine(
               //     "The string is null.");
            }

            // Decimal to string conversion will not overflow.
            stringVal = System.Convert.ToString(decimalVal);
            //System.Console.WriteLine(
              //  "The decimal as a string is {0}.", stringVal);
        }






        private void comboBox_TextChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                int.Parse(comboBox_baud.Text);
            }
            catch (FormatException)
            {
                comboBox_baud.Text = "";
            }



        }

        private void button_updateinfo(object sender, RoutedEventArgs e)
        {
            string Temp_baudrate = comboBox_baud.SelectedIndex.ToString();
            if (comboBox_baud.Text == "")
                return;
            int tmp_baud = int.Parse(comboBox_baud.Text);
            // string COM= comboBox_COM.SelectedIndex.ToString();
            new_comm.BaudRate = tmp_baud;
            try
            {
                new_comm.PortName = comboBox_COM.Text;
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Choose COM");
            }
            label_currentinfo.FontSize = 10;
            label_currentinfo.Content = "Current: Baudrate= \n" + tmp_baud.ToString() + ", " + comboBox_COM.Text;
        }
        

        private void button_openport(object sender, RoutedEventArgs e)
        {
            try
            {
                new_comm.Open();
                if (new_comm.IsOpen == true)
                    textBox_output.Text = "The port is listening";
                //opening new evant handler to read from port
                new_comm.DataReceived += New_comm_DataReceived;
                //new_comm.Write(new byte[] { 0xFA },0,1);  

                
            }
            catch {
                textBox_output.Text = "Failed to open port...";//when i write succsses?

            }
        }

        private void New_comm_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
           
            SerialPort sp = (SerialPort)sender;
            byte[] b = new byte[10];//add event on the gui that changes the size if the byte
            try {
                sp.Read(b, 0, b.Length);
            }
            catch
            {
                New_comm_DataReceived(sender, e);
            }
            
            Application.Current.Dispatcher.Invoke(new Action(() => textBox_output.Text += " \n" + Byte_Convert(b)));
            //add method that checks wich radiobutton is clicked and converts it
            // Application.Current.Dispatcher.Invoke(new Action(() => textBox_output.Text+="\n"));
        }

        private string Byte_Convert(byte [] array  )
        {
            string retval="";
            foreach(byte b in array)
            {
                retval += b.ToString();
            }
            return retval;
        }

        private void button_closeport(object sender, RoutedEventArgs e)
        {
            new_comm.Close();
            textBox_output.Text = "";
        }
    }
}
